# run qunit tests using selenium

import os
import selenium.webdriver as webdriver
import selenium.webdriver.common.by as by
import selenium.webdriver.support.expected_conditions as EC
import selenium.webdriver.support.ui as ui
import sys

mode = sys.argv[1]
if mode == "build":
    test_path = os.getcwd()
elif mode == "autopkgtest":
    test_path = "/usr/share/doc/node-nouislider"
else:
    raise ValueError("unknown mode")

options = webdriver.chrome.options.Options()
options.add_argument("--headless")
options.add_argument("--no-sandbox")

try:
    driver = webdriver.Chrome(options=options)
except:
    print("warning: unable to set up web driver; skipping")
    exit(77 if mode == "autopkgtest" else 0)

driver.get(f"file://{test_path}/tests/slider.html")

ui.WebDriverWait(driver, 10).until(
    EC.presence_of_element_located((by.By.CLASS_NAME, "failed")))

print(driver.find_element(by.By.ID, "qunit-testresult").text)
failed = int(driver.find_element(by.By.CLASS_NAME, "failed").text)

driver.quit()
exit(failed)
